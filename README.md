# picamp

Raspberry PI Zero W based music and lights for camping.

* web control
* jukebox for local music assets
* bluetooth speaker for music output
* RGB PWM light control

## network setup

https://blog.thewalr.us/2017/09/26/raspberry-pi-zero-w-simultaneous-ap-and-managed-mode-wifi/
https://gist.github.com/lukicdarkoo/6b92d182d37d0a10400060d8344f86e4

### flash setup
* flash recent raspbian to microsd and edit
```
cat > /Volumes/boot/wpa_supplicant.conf <<EOF
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
 
network={
    ssid="iso"
    psk="is0lati0n"
}
EOF
touch /Volumes/boot/ssh
```

* unmount drive and install in pi, power on and login (pi/raspberry)
```
curl https://bitbucket.org/laird/picamp/raw/efe29e40fa606ab90168907fd5e72de718010ca6/configure.sh -o configure.sh
chmod 755 curl.sh
./configure -a <APSSID> <APPASS> -c <CLIENTSSID> <CLIENTPASS>
```

